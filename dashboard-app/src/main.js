import Vue from 'vue'
import VueRouter from './router.js'
import './plugins/mixins.js'
import BoostrapVue from 'bootstrap-vue'
import App from './App.vue'
import {VueLogger, VueLoggerOptions} from './logger.js'

Vue.config.productionTip = false

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueLogger, VueLoggerOptions);

Vue.use(BoostrapVue);

new Vue({
  router: VueRouter,
  render: h => h(App),
}).$mount('#app')