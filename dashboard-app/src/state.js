import Vue from 'vue'
import _ from 'lodash'
import moment from 'moment'
// import {$api} from './services/services.js'

export const AppState = Vue.observable({
    initOn: null
});

export const AppStateMutations = {
    async init () {
        AppState.initOn = moment().valueOf();     
    }
    
};

AppStateMutations.init();