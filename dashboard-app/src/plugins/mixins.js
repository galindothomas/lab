import Vue from 'vue'
import {$api} from '../services/services.js'

Vue.mixin({
    computed: {
        $api:() => $api
    }
});