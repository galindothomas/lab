import Vue from 'vue'
import VueRouter from 'vue-router'

import HomePage from './pages/HomePage.vue'
import AboutPage from './pages/AboutPage.vue'
import NotFoundPage from './pages/NotFoundPage.vue'
import DataPage from './pages/DataPage.vue'
import IssuePage from './pages/IssuePage.vue'
import IssuesPage from './pages/IssuesPage.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage
        },
        {
            path: '/about',
            name: 'about',
            component: AboutPage
        },
        {
            path: '/data/:id',
            name: 'data',
            component: DataPage
        },
        {
            path: '/issues',
            name: 'issues',
            component: IssuesPage
        },
        {
            path: '/issue/:id',
            name: 'issue',
            component: IssuePage
        },
        {
            path: '/:catchAll(.*)',
            name: '404',
            component: NotFoundPage
        }
    ]
})