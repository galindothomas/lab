import Vue from 'vue'

export class BaseApiService {
    baseUrl = process.env.VUE_APP_API_ENDPOINT;
    resource;

    constructor(resource) {
        if (!resource) {
            throw new Error("Risorsa non esistente");
        }
        this.resource = resource;
        
    }
    getUrl(id = "") {
        return `${this.baseUrl}/${this.resource}/${id}`;
    }
    handleErrors(error) {
        if (Vue.prototype.$log) {
            Vue.prototype.$log.error("Api errors!!", error);

        } else {
            console.log("Api errors!!", error);

        }
    }
} 
export class ReadOnlyApiService extends BaseApiService {
    constructor(resource) {
        super(resource);
    }
    async getAll() {
        try {
            const response = await fetch(this.getUrl(), {
                method: 'GET',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                }
            });

            return {
                success: response.ok,
                code: response.status,
                message: response.statusText,
                data: await response.json()
            };
        } catch(error) {
            this.handleErrors(error);
        }
    }
    async get(id) {
        try {
            const response = await fetch(this.getUrl(id), {
                method: 'GET',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                }
            });

            return {
                success: response.ok,
                code: response.status,
                message: response.statusText,
                data: await response.json()
            };
        } catch(error) {
            this.handleErrors(error);
        }
    }
}
export class CrudApiService extends ReadOnlyApiService {
    constructor(resource) {
        super(resource);
    }
    async create(data = {}) {
        try {
            const response = await fetch(this.getUrl(), {
                method: 'POST',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify(data)
            });

            return {
                success: response.ok,
                code: response.status,
                message: response.statusText,
                data: await response.json()
            };
        } catch(error) {
            this.handleErrors(error);
        }
    }
    async update(id, data = {}) {
        if (!id) {
            throw new Error("Missing ID");
        }
        try {
            const response = await fetch(this.getUrl(id), {
                method: 'PUT',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify(data)
            });

            return {
                success: response.ok,
                code: response.status,
                message: response.statusText,
                data: await response.json()
            };
        } catch(error) {
            this.handleErrors(error);
        }
    }
    async partialUpdate(id, data = {}) {
        if (!id) {
            throw new Error("Missing ID");
        }
        try {
            const response = await fetch(this.getUrl(id), {
                method: 'PATCH',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify(data)
            });

            return {
                success: response.ok,
                code: response.status,
                message: response.statusText,
                data: await response.json()
            };
        } catch(error) {
            this.handleErrors(error);
        }
    }
    async delete(id) {
        if (!id) {
            throw new Error("Missing ID");
        }
        try {
            const response = await fetch(this.getUrl(id), {
                method: 'DELETE',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                }
            });

            return {
                success: response.ok,
                code: response.status,
                message: response.statusText
            };
        } catch(error) {
            this.handleErrors(error);
        }
    }
    
}