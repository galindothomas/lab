import {CrudApiService} from './base/api.js'

export default class IssuesApiService extends CrudApiService {
    constructor () {
        super('issues');
    }
    
}