import Vue from 'vue'
import _ from 'lodash'
import moment from 'moment'

export const AppState = Vue.observable({issues:[]});

export const AppStateMutations = {
    init () {
        AppState.issues = [
            /* {
                id: 1,
                typeId: 1,
                summary: 'issue 1',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id risus est. Morbi auctor gravida pulvinar. Aliquam lobortis mauris diam, id consequat risus tempus vitae. Proin accumsan lectus a massa aliquet, sed auctor lorem bibendum. Cras dolor felis, accumsan a ipsum varius, ornare fringilla dolor. Suspendisse sodales justo id cursus semper.',
                assignee: 'stefano.ricci@riccimatic.com',
                priorityId: 1,
                createdOn: 1643413038300,
                updatedOn: 1643413429014
            },
            {
                id: 2,
                typeId: 1,
                summary: 'issue 2',
                description: 'Ut quis lacinia tortor. Sed viverra, lectus in hendrerit sagittis, nisi magna ultricies sapien, a lacinia ex ligula vitae ipsum. Maecenas mi turpis, iaculis eu eleifend eu, iaculis a neque.',
                assignee: 'stefano.ricci@riccimatic.com',
                priorityId: 3,
                createdOn: 1643413038300,
                updatedOn: 1643413429014
            },
            {
                id: 3,
                typeId: 2,
                summary: 'issue 3',
                description: 'Aenean mollis lacinia facilisis. Sed suscipit felis neque, fringilla consequat lectus porta et.',
                assignee: 'stefano.ricci@riccimatic.com',
                priorityId: 3,
                createdOn: 1643413038300,
                updatedOn: 1643413429014
            },
            {
                id: 4,
                typeId: 2,
                summary: 'issue 4',
                description: 'Nunc vitae egestas lacus, nec finibus quam. Donec ultricies id turpis sit amet dignissim. Suspendisse ornare pulvinar ex ac accumsan. Donec hendrerit eu lacus gravida gravida. Curabitur et felis felis. Suspendisse aliquet eget sapien sed accumsan. ',
                assignee: 'stefano.ricci@riccimatic.com',
                priorityId: 4,
                createdOn: 1643413038300,
                updatedOn: 1643413429014
            },
            {
                id: 5,
                typeId: 3,
                summary: 'issue 5',
                description: 'Nullam bibendum gravida venenatis. Sed nec aliquam urna, a egestas urna. Quisque pretium, sem ut efficitur auctor, metus magna condimentum tellus, a feugiat elit libero sed augue. ',
                assignee: 'stefano.ricci@riccimatic.com',
                priorityId: 3,
                createdOn: 1643413038300,
                updatedOn: 1643413429014
                } */
            ];
    },
    countIssues() {
        if (AppState.issues)
            return AppState.issues.length;
        else 
            return 0;
    },
    getIssues() {
        return AppState.issues;
    },
    getIssuesClone() {
        return _.cloneDeep(AppState.issues);
    },
    getIssue(id) {
        if (id >= 0) {
            let issue = _.find(AppState.issues, function (x) {
                return x.id == id;
            });
    
            if (issue == undefined) {
                return null;
            } else {
                return issue;
            }
        } else {
            return null;
        }
    },
    getIssueClone(id) {
        let issue = this.getIssue(id);

        if (issue == null) {
            return null;
        } else {
            return _.cloneDeep(issue);
        }
    },
    getIssuesTypes() {
        return [
                { value: 1, label: 'Task' },
                { value: 2, label: 'Story' },
                { value: 3, label: 'Bug' },
            ];
    },
    getIssuesPriorities() {
        return [
            { value: 1, label: 'Highest' },
            { value: 2, label: 'High' },
            { value: 3, label: 'Medium' },
            { value: 4, label: 'Low' },
            { value: 5, label: 'Lowest' },
        ];
    },
    deleteIssue : function (id) { 
        
        AppState.issues = _.remove(AppState.issues, function(x) {
            return x.id != id;
        });

    },
    updateIssue : function (id, typeId, summary, description, assignee, priorityId) {
        let issue = _.find(AppState.issues, function (x) {
            return x.id == id;
        });

        issue.typeId = typeId;
        issue.summary = summary;
        issue.description = description;
        issue.assignee = assignee;
        issue.priorityId = priorityId;
        issue.updatedOn = moment().valueOf();
    },
    updateObject : function (obj) {
        let issue = _.find(AppState.issues, function (x) {
            return x.id == obj.id;
        });

        issue.typeId = obj.typeId;
        issue.summary = obj.summary;
        issue.description = obj.description;
        issue.assignee = obj.assignee;
        issue.priorityId = obj.priorityId;
        issue.updatedOn = obj.updatedOn;
    },
    addIssue : function (typeId, summary, description, assignee, priorityId) {
        let maxId = AppState.issues.length;

        if (maxId > 0) {
            maxId = Math.max.apply(null, AppState.issues.map(function(item) {
                return item.id;
            }));
        }
        AppState.issues.push({
            id: maxId+1,
            typeId: typeId,
            summary: summary,
            description: description,
            assignee: assignee,
            priorityId: priorityId,
            createdOn: moment().valueOf(),
            updatedOn: moment().valueOf()
        });
        
    },
    clearIssues: function () {
        AppState.issues = [];
    },
    addIssues: function (arr) {
        AppState.issues = arr;
    },
    addIssueObject: function (obj) {
        AppState.issues.push(obj);
    }
};

AppStateMutations.init();