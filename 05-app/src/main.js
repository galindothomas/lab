import Vue from 'vue'
import VueLogger from 'vuejs-logger'
import VueRouter from './router.js'
import BoostrapVue from 'bootstrap-vue'
import App from './App.vue'

Vue.config.productionTip = false

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BoostrapVue);

// logLevels :  ['debug', 'info', 'warn', 'error', 'fatal']
const isProduction = process.env.NODE_ENV === 'production';
 
const options = {
    isEnabled: true,
    logLevel : isProduction ? 'error' : 'debug',
    stringifyArguments : false,
    showLogLevel : true,
    showMethodName : true,
    separator: '|',
    showConsoleColors: true
};

Vue.use(VueLogger, options);

new Vue({
  router: VueRouter,
  render: h => h(App),
}).$mount('#app')