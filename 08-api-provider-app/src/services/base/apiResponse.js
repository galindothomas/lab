
export default class ApiResponse {
    success = false;
    code = 500;
    message = "";
    data = null;

    constructor (success, code, message, data = null) {
        this.success = success;
        this.code = code;
        this.message = message;
        this.data = data;

    }
}