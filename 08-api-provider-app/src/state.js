import Vue from 'vue'
import _ from 'lodash'
import moment from 'moment'
import {$api} from './services/services.js'

export const AppState = Vue.observable({
    issues: null,
    initOn: null
});

export const AppStateMutations = {
    async init () {
        AppState.initOn = moment().valueOf();
        AppState.issues = [];
        let responseApi = await $api.issues.getAll();
        if (responseApi.success) {
            AppState.issues = responseApi.data;
        }
    },
    countIssues() {
        if (AppState.issues)
            return AppState.issues.length;
        else 
            return 0;
    },
    getIssues() {
        return AppState.issues;
    },
    getIssuesClone() {
        return _.cloneDeep(AppState.issues);
    },
    getIssue(id) {
        if (id >= 0) {
            let issue = _.find(AppState.issues, function (x) {
                return x.id == id;
            });
    
            if (issue == undefined) {
                return null;
            } else {
                return issue;
            }
        } else {
            return null;
        }
    },
    getIssueClone(id) {
        let issue = this.getIssue(id);

        if (issue == null) {
            return null;
        } else {
            return _.cloneDeep(issue);
        }
    },
    getIssuesTypes() {
        return [
                { value: 1, label: 'Task' },
                { value: 2, label: 'Story' },
                { value: 3, label: 'Bug' },
            ];
    },
    getIssuesPriorities() {
        return [
            { value: 1, label: 'Highest' },
            { value: 2, label: 'High' },
            { value: 3, label: 'Medium' },
            { value: 4, label: 'Low' },
            { value: 5, label: 'Lowest' },
        ];
    },
    deleteIssue : async function (id) { 
        let response = await $api.issues.delete(id);
        if (response.success) {
            AppState.issues = _.remove(AppState.issues, function(x) {
                return x.id != id;
            });
        }

    },
    updateIssue : async function (id, typeId, summary, description, assignee, priorityId) {
        let issue = _.find(AppState.issues, function (x) {
            return x.id == id;
        });

        let response = await $api.issues.update(id, {
            id: id,
            typeId: typeId,
            summary: summary,
            description: description,
            assignee: assignee,
            priorityId: priorityId,
            createdOn: issue.createdOn,
            updatedOn: moment().valueOf()
        });
        if (response.success) {
            issue.typeId = typeId;
            issue.summary = summary;
            issue.description = description;
            issue.assignee = assignee;
            issue.priorityId = priorityId;
            issue.createdOn = response.data.createdOn;
            issue.updatedOn = response.data.updatedOn;
        }
    },
    addIssue : async function (typeId, summary, description, assignee, priorityId) {
        let response = await $api.issues.create({
            typeId: typeId,
            summary: summary,
            description: description,
            assignee: assignee,
            priorityId: priorityId,
            createdOn: moment().valueOf(),
            updatedOn: moment().valueOf()
        });
        if (response.success) {
            AppState.issues.push(response.data);
            return response.data.id;   
        }
        return null;
    },
    clearIssues: function () {
        AppState.issues = [];
    },
    addIssues: function (arr) {
        AppState.issues = arr;
    }
};

AppStateMutations.init();