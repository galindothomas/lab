import Vue from 'vue'
import VueLogger from 'vuejs-logger';
import App from './App.vue'

Vue.config.productionTip = false

// logLevels :  ['debug', 'info', 'warn', 'error', 'fatal']

const isProduction = process.env.NODE_ENV === 'production';
 
const options = {
    isEnabled: true,
    logLevel : isProduction ? 'error' : 'debug',
    stringifyArguments : false,
    showLogLevel : true,
    showMethodName : true,
    separator: '|',
    showConsoleColors: true
};

Vue.use(VueLogger, options);

// end logger

new Vue({
  render: h => h(App),
}).$mount('#app')

