export const isNullOrWhiteSpace = (text) => {
    // if (!!str)
    // It returns false for null, undefined, 0, 000, "", false.
    // It returns true for all string values other than the empty string (including strings like "0" and " ")

    let check = !!text;

    if(check){
        if(text.trim() == '') {
            return true;
        } else {
            return false;
        }
    } else {
        return true;
    }
}

export const isValidEmail = (email) => {
    //eslint-disable-next-line
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}