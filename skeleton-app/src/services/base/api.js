import Vue from 'vue'
import ApiResponse from './apiResponse';

export class BaseApiService {
    baseUrl = process.env.VUE_APP_API_ENDPOINT;
    resource;

    constructor(resource) {
        if (!resource) {
            throw new Error("Risorsa non esistente");
        }
        this.resource = resource;
        
    }
    getUrl(id = "") {
        return `${this.baseUrl}/${this.resource}/${id}`;
    }
    handleErrors(error) {
        if (Vue.prototype.$log) {
            Vue.prototype.$log.error("Api errors!!", error);

        } else {
            console.log("Api errors!!", error);

        }
    }
    handleDebug(message) {
        if (Vue.prototype.$log) {
            Vue.prototype.$log.debug("Api debug!!", message);

        }
    }
} 
export class ReadOnlyApiService extends BaseApiService {
    constructor(resource) {
        super(resource);
    }
    async getAll() {
        try {
            this.handleDebug("Url: " + this.getUrl());
            const response = await fetch(this.getUrl(), {
                method: 'GET',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                }
            });
            return new ApiResponse(response.ok, response.status, response.statusText, await response.json());
                
        } catch(error) {
            this.handleErrors(error);
            return new ApiResponse(false, 500, 'Global error: ' + error);

        }
    }
    async get(id) {
        try {
            this.handleDebug("Url: " + this.getUrl(id));
            const response = await fetch(this.getUrl(id), {
                method: 'GET',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                }
            });
            return new ApiResponse(response.ok, response.status, response.statusText, await response.json());

        } catch(error) {
            this.handleErrors(error);
            return new ApiResponse(false, 500, 'Global error: ' + error);
            
        }
    }
}
export class CrudApiService extends ReadOnlyApiService {
    constructor(resource) {
        super(resource);
    }
    async create(data = {}) {
        try {
            this.handleDebug("Url: " + this.getUrl());
            this.handleDebug("Data: " + data);
            const response = await fetch(this.getUrl(), {
                method: 'POST',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify(data)
            });
            return new ApiResponse(response.ok, response.status, response.statusText, await response.json());

        } catch(error) {
            this.handleErrors(error);
            return new ApiResponse(false, 500, 'Global error: ' + error);

        }
    }
    async update(id, data = {}) {
        if (!id) {
            throw new Error("Missing ID");
        }
        try {
            this.handleDebug("Url: " + this.getUrl(id));
            this.handleDebug("Data: " + data);
            const response = await fetch(this.getUrl(id), {
                method: 'PUT',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify(data)
            });
            return new ApiResponse(response.ok, response.status, response.statusText, await response.json());

        } catch(error) {
            this.handleErrors(error);
            return new ApiResponse(false, 500, 'Global error: ' + error);
            
        }
    }
    async partialUpdate(id, data = {}) {
        if (!id) {
            throw new Error("Missing ID");
        }
        try {
            this.handleDebug("Url: " + this.getUrl(id));
            this.handleDebug("Data: " + data);
            const response = await fetch(this.getUrl(id), {
                method: 'PATCH',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                },
                body: JSON.stringify(data)
            });
            return new ApiResponse(response.ok, response.status, response.statusText, await response.json());

        } catch(error) {
            this.handleErrors(error);
            return new ApiResponse(false, 500, 'Global error: ' + error);

        }
    }
    async delete(id) {
        if (!id) {
            throw new Error("Missing ID");
        }
        try {
            this.handleDebug("Url: " + this.getUrl(id));
            const response = await fetch(this.getUrl(id), {
                method: 'DELETE',
                headers: {
                    'Accept' : 'application/json',
                    'Content-type' : 'application/json'
                }
            });
            return new ApiResponse(response.ok, response.status, response.statusText);

        } catch(error) {
            this.handleErrors(error);
            return new ApiResponse(false, 500, 'Global error: ' + error);

        }
    }
    
}